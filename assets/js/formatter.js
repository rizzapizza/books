function Formatter() {}
Formatter.prettifyHeader = function(str) {
  return str
    .replace(/([a-z])([A-Z])/g, '$1 $2')
    .replace(/([a-z])([0-9])/g, '$1 $2')
    .replace(/^./, function (str) {
      return str.toUpperCase();
    });
};
Formatter.formatDate = function(str) {
  return str.substr(0, 'yyyy-mm-dd'.length);
};
Formatter.pluralize = function(str) {
  return str + 's';
};
Formatter.capitalizeFirst = function(str) {
  return str.substr(0, 1).toUpperCase() + str.substr(1, str.length);
}
