$(document).foundation();

var AuthorsView = function() {
  RestView.call(this, 'author');
};

inheritsFrom(AuthorsView, RestView);

var authorsView = new AuthorsView();
authorsView.showAll();

$('body').on('click', '.back', function(event) {
  var target = event.target ? event.target : event.srcElement;
  $('#' + target.getAttribute('toHide')).hide();
  authorsView.showAll();

  event.preventDefault();
});

$('body').on('click', '.show', function(event) {
  console.log('Show clicked.');

  var target = event.target ? event.target : event.srcElement;
  authorsView.showDetails(target.getAttribute('targetId'));

  event.preventDefault();
});

$('body').on('click', '.new', function(event) {
  authorsView.showNewForm();

  event.preventDefault();
});

$('body').on('click', '.create', function(event) {
  authorsView.create();

  event.preventDefault();
});


$('body').on('click', '.edit', function(event) {
  var target = event.target ? event.target : event.srcElement;
  authorsView.showEditForm(target.getAttribute('targetId'));

  event.preventDefault();
});

$('body').on('click', '.delete', function(event) {
  var target = event.target ? event.target : event.srcElement;
  authorsView.delete(target.getAttribute('targetId'));

  event.preventDefault();
});

$('body').on('click', '.update', function(event) {
  authorsView.update();

  event.preventDefault();
});

$('body').on('click', '.search', function(event) {
  authorsView.search();

  event.preventDefault();
});