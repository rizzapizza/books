function Row(object) {

  this.tr = document.createElement('tr');

  for (var field in object) {
    if (field === 'id') {
      this.id = object[field];
    } else {
      var td = document.createElement('td');
      var text = document.createTextNode(object[field]);
      td.appendChild(text);
      this.tr.appendChild(td);
    }
  }

  var buttons = [new ShowButton(this.id), new EditButton(this.id), new DeleteButton(this.id)];
  for (var i in buttons) {
    var td = document.createElement('td');
    buttons[i].appendTo(td);
    this.tr.appendChild(td);
  }
}
Row.prototype.appendTo = function(parent) {
  parent.appendChild(this.tr);
};

function HeaderRow(object) {
  this.tr = document.createElement('tr');

  for (var field in object) {
    if (field != 'id') {
      var th = document.createElement('th');
      var text = document.createTextNode(Formatter.prettifyHeader(field));
      th.appendChild(text);
      this.tr.appendChild(th);
    }
  }

  for (var i = 0; i < 3; i++) {
    var th = document.createElement('th');
    this.tr.appendChild(th);
  }
}
HeaderRow.prototype.appendTo = function(parent) {
  parent.appendChild(this.tr);
};

function Table(objects) {
  this.table = document.createElement('table');
  this.thead = document.createElement('thead');
  this.tbody = document.createElement('tbody');

  var header = document.createElement('tr');
  for (var i in objects) {
    if (i == 0) {
      var headerRow = new HeaderRow(objects[i]);
      headerRow.appendTo(this.thead);
    }
    var row = new Row(objects[i]);
    row.appendTo(this.tbody);
  }

  this.table.appendChild(this.thead);
  this.table.appendChild(this.tbody);
}
Table.prototype.appendTo = function(parent) {
  parent.append(this.table);
};
