$(document).foundation();

var BooksView = function() {
  RestView.call(this, 'book');
};

inheritsFrom(BooksView, RestView);

BooksView.prototype.showDetails = function(id) {
  $.getJSON(this.baseUrl + '/' + id, function(book) {
    for (var field in book) {
      if (field === 'author' || field === 'publisher') {
        $('#' + field).html(book[field].name);
      } else if (field === 'genres') {
        $('#' + field).html('');
        book.genres.forEach(function(genre, index) {
          $('#' + field).append('<li>' + genre.name + '</li>');
        });
      } else {
        $('#' + field).html(book[field]);
      }
    }

    $(this.mainDivId).hide();
    $(this.showDivId).show();
  }.bind(this));
};

BooksView.prototype.showNewForm = function() {
  $('input[type != "submit"]').val('');

  $.getJSON('/authors', function(authors) {
    $('.authors[formId = "createBook"]').html('');
    for (var i in authors) {
      $('.authors[formId = "createBook"]').append('<option value = "' + authors[i].id + '">' + authors[i].name + '</option>');
    }
  });

  $.getJSON('/publishers', function(publishers) {
    $('.publishers[formId = "createBook"]').html('');
    for (var i in publishers) {
      $('.publishers[formId = "createBook"]').append('<option value = "' + publishers[i].id + '">' + publishers[i].name + '</option>');
    }
  });
  $.getJSON('/genres', function(genres) {
    $('.genres[formId = "createBook"]').html('');
    for (var i in genres) {
      $('.genres[formId = "createBook"]').append('<input type = "checkbox" name = "genres" value = "' + genres[i].id + '" />' + genres[i].name);
    }
  });

  $(this.mainDivId).hide();
  $(this.newDivId).show();
};

BooksView.prototype.showEditForm = function(id) {
  $(this.updateFormId).attr('bookId', id);
  $(this.errorDivClass).html('');
  $.getJSON('/books/' + id, function(book) {
    for (var field in book) {
      if (field === 'publishedAt') {
        $('.publishedAt[formId = "updateBook"]').val(Formatter.formatDate(book[field]));
      } else if (field === 'author') {
        $('.authors' + '[formId = "updateBook"]').html('');
        $.getJSON('/authors', function (authors) {
          authors.forEach(function (author, index) {
            if (author.id === book.author.id) {
              $('.authors' + '[formId = "updateBook"]').append('<option selected = "selected" value = "' + author.id + '">' + author.name + '</option>');
            } else {
              $('.authors' + '[formId = "updateBook"]').append('<option value = "' + author.id + '">' + author.name + '</option>');
            }
          });
        });
      } else if (field === 'publisher') {
        $('.publishers' + '[formId = "updateBook"]').html('');
        $.getJSON('/publishers', function (publishers) {
          publishers.forEach(function (publisher, index) {
            if (publisher.id === book.publisher.id) {
              $('.publishers' + '[formId = "updateBook"]').append('<option selected = "selected" value = "' + publisher.id + '">' + publisher.name + '</option>');
            } else {
              $('.publishers' + '[formId = "updateBook"]').append('<option value = "' + publisher.id + '">' + publisher.name + '</option>');
            }
          });
        });
      } else if (field === 'genres') {
        $('.genres' + '[formId = "updateBook"]').html('');
        $.getJSON('/genres', function(genres) {
          genres.forEach(function (genre, index) {
            if (isObjectInArray(genre, book.genres)) {
              $('.genres[formId = "updateBook"]').append('<input name = "genres" type = "checkbox" checked = "checked" value = "' + genre.id + '" />' + genre.name);
            } else {
              $('.genres[formId = "updateBook"]').append('<input name = "genres" type = "checkbox" value = "' + genre.id + '" />' + genre.name);
            }
          });
        });
      } else {
        $('.' + field + '[formId = "updateBook"]').val(book[field]);
      }
    }
    $(this.mainDivId).hide();
    $(this.editDivId).show();
  }.bind(this));
};

BooksView.prototype.shouldUpdate = function() {
  var inputs = $('#updateBook :input');
  var foundGenres = false;
  inputs.each(function() {
    if ($(this).attr('name') === 'genres' && $(this).is(':checked')) {
      console.log('Found checked genres');
      foundGenres = true;
    }
  });
  if (!foundGenres) {
    $(this.errorDivClass).html('At least one genre must be checked.');
  }
  return foundGenres;
};

var booksView = new BooksView();
booksView.showAll();

$('body').on('click', '.back', function(event) {
  var target = event.target ? event.target : event.srcElement;
  $('#' + target.getAttribute('toHide')).hide();
  booksView.showAll();

  event.preventDefault();
});

$('body').on('click', '.show', function(event) {
  var target = event.target ? event.target : event.srcElement;
  booksView.showDetails(target.getAttribute('targetId'));

  event.preventDefault();
});

$('body').on('click', '.new', function(event) {
  booksView.showNewForm();

  event.preventDefault();
});

$('body').on('click', '.create', function(event) {
  booksView.create();

  event.preventDefault();
});


$('body').on('click', '.edit', function(event) {
  var target = event.target ? event.target : event.srcElement;
  booksView.showEditForm(target.getAttribute('targetId'));

  event.preventDefault();
});

$('body').on('click', '.delete', function(event) {
  var target = event.target ? event.target : event.srcElement;
  booksView.delete(target.getAttribute('targetId'));

  event.preventDefault();
});

$('body').on('click', '.update', function(event) {
  booksView.update();

  event.preventDefault();
});

$('body').on('click', '.search', function(event) {
  booksView.search();

  event.preventDefault();
});
