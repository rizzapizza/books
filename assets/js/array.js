function haveSameValues(objA, objB) {
  for (var field in objA) {
    if (objA[field] != objB[field]) {
      return false;
    }
  }
  if (Object.keys(objA).length != Object.keys(objB).length) {
    return false;
  }
  return true;
}

function isObjectInArray(obj, arr) {
  for (var i = 0; i < arr.length; i++) {
    if (haveSameValues(obj, arr[i])) {
      return true;
    }
  }
  return false;
}