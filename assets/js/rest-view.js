var RestView = function (singularName) {
  this.attrId = singularName + 'Id';
  this.baseUrl = '/' + Formatter.pluralize(singularName);
  this.tableId = '#' + Formatter.pluralize(singularName) + 'Table';
  this.mainDivId = '#' + Formatter.pluralize(singularName);
  this.editDivId = '#edit' + Formatter.capitalizeFirst(singularName);
  this.showDivId = '#show' + Formatter.capitalizeFirst(singularName);
  this.newDivId = '#new' + Formatter.capitalizeFirst(singularName);
  this.updateFormId = '#update' + Formatter.capitalizeFirst(singularName);
  this.createFormId = '#create' + Formatter.capitalizeFirst(singularName);
  this.searchFormId = '#search' + Formatter.capitalizeFirst(Formatter.pluralize(singularName));
  this.errorDivClass = '.error';
};

RestView.prototype.showAll = function() {
  $(this.tableId).html('');
  $.getJSON(this.baseUrl, function(objects) {
    if (objects.length > 0) {
      var table = new Table(objects);
      table.appendTo($(this.tableId));
    } else {
      $(this.tableId).append('<p>None to display.</p>');
    }
    $(this.mainDivId).show();
  }.bind(this));
};

RestView.prototype.getDetails = function(id, cb) {
  $.getJSON(this.baseUrl + '/' + id, cb);
};

RestView.prototype.showDetails = function(id) {
  this.getDetails(id, function(object) {
    for (var field in object) {
      $('#' + field).html(object[field]);
    }

    $(this.mainDivId).hide();
    $(this.showDivId).show();
  }.bind(this));
};

RestView.prototype.showNewForm = function() {
  $('input[type != "submit"]').val('');

  $(this.mainDivId).hide();
  $(this.newDivId).show();
};

RestView.prototype.displayError = function(jqXHR, status) {
  var error = jqXHR.responseJSON.error;
  var message;
  if (error.parameters) {
    message = error.parameters.description;
  } else {
    message = error.message;
  }
  $(this.errorDivClass).html(message);
};

RestView.prototype.shouldCreate = function() { return true; };

RestView.prototype.create = function() {
  if (this.shouldCreate()) {
    var form = $(this.createFormId);
    $.ajax({
      method: 'post',
      url: this.baseUrl,
      data: form.serialize(),
      dataType: 'json'
    }).done(function(created) {
      $(this.newDivId).hide();
      $(this.errorDivClass).html('');

      this.showAll();
    }.bind(this)).fail(this.displayError.bind(this));
  }
};

RestView.prototype.showEditForm = function(id) {
  $(this.updateFormId).attr(this.attrId, id);
  $(this.errorDivClass).html('');

  this.getDetails(id, function(object) {
    for (var field in object) {
      $('.' + field + '[formId = "' + this.updateFormId.substr(1, this.updateFormId.length) + '"]').val(object[field]);
    }
    $(this.mainDivId).hide();
    $(this.editDivId).show();
  }.bind(this));
};

RestView.prototype.shouldUpdate = function() { return true; };

RestView.prototype.update = function() {
  if (this.shouldUpdate()) {
    var form = $(this.updateFormId);

    $.ajax({
      method: 'put',
      url: this.baseUrl + '/' + form.attr(this.attrId),
      data: form.serialize(),
      dataType: 'json'
    }).done(function(updated) {
      $(this.editDivId).hide();
      $(this.errorDivClass).html('');

      this.showAll();
    }.bind(this)).fail(this.displayError.bind(this));
  }
};

RestView.prototype.search = function() {
  var form = $(this.searchFormId);

  $.ajax({
    method: 'get',
    url: this.baseUrl,
    data: form.serialize(),
    dataType: 'json'
  }).done(function(objects) {
    $(this.tableId).html('');
    if (objects.length > 0) {
      var table = new Table(objects);
      table.appendTo($(this.tableId));
    } else {
      $(this.tableId).append('<p>None to display.</p>');
    }
  }.bind(this));
};

RestView.prototype.delete = function(id) {
  $.ajax({
    method: 'delete',
    url: this.baseUrl + '/' + id
  }).done(function(result) {
    this.showAll();
  }.bind(this));
};

var inheritsFrom = function(child, parent) {
  child.prototype = Object.create(parent.prototype);
};