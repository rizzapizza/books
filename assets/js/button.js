function ShowButton(targetId) {
  this.button = document.createElement('a');

  var buttonText = document.createTextNode('Show');
  this.button.appendChild(buttonText);

  this.button.setAttribute('href', '#');
  this.button.setAttribute('class', 'show button');
  this.button.setAttribute('targetId', targetId);
}
ShowButton.prototype.appendTo = function(parent) {
  parent.appendChild(this.button);
};

function EditButton(targetId) {
  this.button = document.createElement('a');

  var buttonText = document.createTextNode('Edit');
  this.button.appendChild(buttonText);

  this.button.setAttribute('href', '#');
  this.button.setAttribute('class', 'edit button');
  this.button.setAttribute('targetId', targetId);
}
EditButton.prototype.appendTo = function(parent) {
  parent.appendChild(this.button);
};

function DeleteButton(targetId) {
  this.button = document.createElement('a');

  var buttonText = document.createTextNode('Delete');
  this.button.appendChild(buttonText);

  this.button.setAttribute('href', '#');
  this.button.setAttribute('class', 'delete alert button');
  this.button.setAttribute('targetId', targetId);
}
DeleteButton.prototype.appendTo = function(parent) {
  parent.appendChild(this.button);
};
