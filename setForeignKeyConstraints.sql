﻿alter table book
add constraint book_author_fkey foreign key (author)
references author (id)
on update cascade
on delete cascade;

alter table book
add constraint book_publisher_fkey foreign key (publisher)
references publisher (id)
on update cascade
on delete cascade;