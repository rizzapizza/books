require('sails-test-helper');

var validate = require('validate.js');

var rekuire = require('rekuire');
var BookController = rekuire('BookController');

var expect = chai.expect;

describe('BookController.js', function() {
  var sandbox;
  var res;
  var book, books;
  var serverErrorResponse;

  before(function() {
    book = {};
    books = [book];
    serverErrorResponse = {
      'error': {
        'code': '-7',
        'message': 'An error while interacting with the database occured',
      }
    };
  });

  beforeEach(function() {
    sandbox = sinon.sandbox.create();
    res = {
      ok: function() { },
      serverError: function() { },
      created: function() { },
      badRequest: function() { }
    };
  });

  afterEach(function() {
    sandbox.restore();
  });

  describe('#getBook', function() {
    context('book exists', function() {
      it('should call res.ok with the book matching the given id', function() {
        var req = {
          param: function(name) {
            if (name === 'id') {
              return '8';
            } else {
              return undefined;
            }
          }
        };
        var book = {};
        var counters = {
          'author' : 0,
          'publisher': 0,
          'genres': 0
        };
        var execStub = function() {
          var ret = {};
          ret.populate = function (attr) {
            counters[attr]++;
            return ret;
          };
          ret.exec = function (cb) {
            return cb(null, book);
          };
          return ret;
        }();
        sandbox.stub(Book, 'findOne', function(criteria) {
          expect(criteria).to.deep.equal({id: req.param('id')});
          return execStub;
        });
        var okSpy = sandbox.spy(res, 'ok');

        BookController.getBook(req, res);

        for (var attr in counters) {
          console.log(attr + ': ' + counters[attr]);
          expect(counters[attr]).to.deep.equal(1);
        }

        okSpy.should.be.calledOnce;
        okSpy.should.be.calledWith(book);
      });
    });

    context('book does not exist', function() {
      it('should call res.badRequest with a specific error object', function() {
        var req = {
          param: function(name) {
            if (name === 'id') {
              return '9';
            } else {
              return undefined;
            }
          }
        };
        var book = {};
        var execStub = {
          populate: function(author) {
            expect(author).to.deep.equal('author');
            return {
              populate: function(publisher) {
                expect(publisher).to.deep.equal('publisher');
                return {
                  populate: function(genres) {
                    expect(genres).to.deep.equal('genres');
                    return {
                      exec: function(cb) {
                        return cb(null, null);
                      }
                    };
                  }
                };
              }
            };
          }
        };
        sandbox.stub(Book, 'findOne', function(criteria) {
          expect(criteria).to.deep.equal({id: req.param('id')});
          return execStub;
        });
        var errorSpy = sandbox.spy(res, 'badRequest');

        BookController.getBook(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(sails.config.errors.nonexistentEntry);
      });
    });
  });

  describe('#getListOfBooks', function() {
    context('successful fetch from the database', function() {
      it('should call res.ok with list of books', function() {
        var req = {query: {}};
        var results = {rows: []};
        var sqlQuery = {text: 'SELECT * FROM book', values: []};
        sandbox.stub(BookSqlQuery, 'form', function(query) {
          expect(query).to.deep.equal(req.query);
          return sqlQuery;
        });
        sandbox.stub(Book, 'query', function(inputSqlQuery, cb) {
          expect(inputSqlQuery).to.deep.equal(sqlQuery);
          cb(null, results);
        });

        var okSpy = sandbox.spy(res, 'ok');

        BookController.getListOfBooks(req, res);

        okSpy.should.be.calledOnce;
        okSpy.should.be.calledWith(results.rows);
      });
    });

    context('database error', function() {
      it('should call res.serverError with error object', function() {
        var req = {query: {}};
        var sqlQuery = {text: 'SELECT * FROM book', values: []};
        sandbox.stub(BookSqlQuery, 'form', function(query) {
          expect(query).to.deep.equal(req.query);
          return sqlQuery;
        });
        sandbox.stub(Book, 'query', function(inputSqlQuery, cb) {
          expect(inputSqlQuery).to.deep.equal(sqlQuery);
          cb('Error', null);
        });

        var errorSpy = sandbox.spy(res, 'serverError');

        BookController.getListOfBooks(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(sails.config.errors.databaseError);
      });
    });

    context('with database error', function() {
      it('should call res.serverError with error object', function() {

      });
    });
  });

  describe('#createBook', function() {
    context('req.body contains complete and valid parameters', function() {
      it('should return the created book', function() {
        var req = {
          body: {
            title: 'Title',
            author: '1',
            publisher: '1',
            genres: ['1'],
            publishedAt: '2015-08-05',
            isbn10: '1234567890',
            isbn13: '1234567890987'
          }
        };
        var createdBook = {};
        var execStub = {
          exec: function(cb) {
            return cb(null, createdBook);
          }
        };
        sandbox.stub(Book, 'create', function(bookToCreate) {
          expect(bookToCreate).to.deep.equal(req.body);
          return execStub;
        });
        var createdSpy = sandbox.spy(res, 'created');

        BookController.createBook(req, res);

        createdSpy.should.be.calledOnce;
        createdSpy.should.be.calledWith(createdBook);
      });
    });

    context('one attribute is missing', function() {
      it('should return an error saying that the title is missing', function() {
        var req = {
          body: {
            author: '1',
            publisher: '1',
            genres: ['1'],
            publishedAt: '2015-08-05',
            isbn10: '1234567890',
            isbn13: '1234567890987'
          }
        };
        var badRequestResponse = {
          'error': {
            'code': '-1',
            'message': 'Missing or invalid parameters',
            'parameters': {
              'field': 'title',
              'description': 'Title can\'t be blank'
            }
          }
        };
        var errorSpy = sandbox.spy(res, 'badRequest');

        BookController.createBook(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(badRequestResponse);
      });

      it('should return an error saying that the author is missing', function() {
        var req = {
          body: {
            title: 'Title',
            publisher: '1',
            genres: ['1'],
            publishedAt: '2015-08-05',
            isbn10: '1234567890',
            isbn13: '1234567890987'
          }
        };
        var badRequestResponse = {
          'error': {
            'code': '-1',
            'message': 'Missing or invalid parameters',
            'parameters': {
              'field': 'author',
              'description': 'Author can\'t be blank'
            }
          }
        };
        var errorSpy = sandbox.spy(res, 'badRequest');

        BookController.createBook(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(badRequestResponse);
      });
    });

    context('one attribute is invalid', function() {
      it('should return an error saying that the author should be numeric', function() {
        var req = {
          body: {
            title: 'Title',
            author: 'a',
            publisher: '1',
            genres: ['1'],
            publishedAt: '2015-08-05',
            isbn10: '1234567890',
            isbn13: '1234567890987'
          }
        };
        var badRequestResponse = {
          'error': {
            'code': '-1',
            'message': 'Missing or invalid parameters',
            'parameters': {
              'field': 'author',
              'description': 'Author is not a number'
            }
          }
        };
        var errorSpy = sandbox.spy(res, 'badRequest');

        BookController.createBook(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(badRequestResponse);
      });

      it('should return an error saying that the isbn10 length is not right', function() {
        var req = {
          body: {
            title: 'Title',
            author: '1',
            publisher: '1',
            genres: ['1'],
            publishedAt: '2015-08-05',
            isbn10: '12345678901',
            isbn13: '1234567890987'
          }
        };
        var badRequestResponse = {
          'error': {
            'code': '-1',
            'message': 'Missing or invalid parameters',
            'parameters': {
              'field': 'isbn10',
              'description': 'Isbn10 is the wrong length (should be 10 characters)'
            }
          }
        };
        var errorSpy = sandbox.spy(res, 'badRequest');

        BookController.createBook(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(badRequestResponse);
      });

      it('should return an error saying that date has an invalid format', function() {
        var req = {
          body: {
            title: 'Title',
            author: '1',
            publisher: '1',
            genres: ['1'],
            publishedAt: 'blabla',
            isbn10: 'ajfdku88fgg',
            isbn13: 'ajfdku88fg666'
          }
        };
        var badRequestResponse = {
          'error': {
            'code': '-1',
            'message': 'Missing or invalid parameters',
            'parameters': {
              'field': 'publishedAt',
              'description': 'Published at must be a valid date'
            }
          }
        };
        var errorSpy = sandbox.spy(res, 'badRequest');

        BookController.createBook(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(badRequestResponse);
      });
    });
  });

  describe('#updateBook', function() {
    context('book exists', function() {
      context('req.body has valid and complete parameters', function() {
        it('should call res.ok with the updated book', function() {
          var req = {
            body: {
              title: 'Title',
              author: '1',
              publisher: '1',
              genres: ['1'],
              publishedAt: '2015-08-05',
              isbn10: '1234567890',
              isbn13: '1234567890987'
            },
            param: function(name) {
              if (name === 'id') {
                return '3';
              } else {
                return undefined;
              }
            }
          };
          var updatedBooks = [{}];
          var execStub = {
            exec: function(cb) {
              return cb(null, updatedBooks);
            }
          };
          sandbox.stub(Book, 'update', function(bookToUpdate, newBookInfo) {
            expect(bookToUpdate).to.deep.equal({id: '3'});
            expect(newBookInfo).to.deep.equal(req.body);
            return execStub;
          });
          var okSpy = sandbox.spy(res, 'ok');

          BookController.updateBook(req, res);

          okSpy.should.be.calledOnce;
          okSpy.should.be.calledWith(updatedBooks[0]);
        });
      });

      context('req.body has valid but incomplete parameters', function() {
        it('should call res.ok with the updated book', function() {
          var req = {
            body: {
              title: 'A Different Title',
            },
            param: function(name) {
              if (name === 'id') {
                return '3';
              } else {
                return undefined;
              }
            }
          };
          var updatedBooks = [{}];
          var execStub = {
            exec: function(cb) {
              return cb(null, updatedBooks);
            }
          };
          sandbox.stub(Book, 'update', function(bookToUpdate, newBookInfo) {
            expect(bookToUpdate).to.deep.equal({id: '3'});
            expect(newBookInfo).to.deep.equal(req.body);
            return execStub;
          });
          var okSpy = sandbox.spy(res, 'ok');

          BookController.updateBook(req, res);

          okSpy.should.be.calledOnce;
          okSpy.should.be.calledWith(updatedBooks[0]);
        });
      });

      context('req.body has no valid parameters', function() {
        it('should return an error object', function() {
          var req = {
            body: {
              uselessField: 'Useless field'
            },
            param: function(name) {
              if (name === 'id') {
                return '3';
              } else {
                return undefined;
              }
            }
          };

          var errorSpy = sandbox.spy(res, 'badRequest');

          BookController.updateBook(req, res);

          errorSpy.should.be.calledOnce;
          errorSpy.should.be.calledWith(sails.config.errors.badRequest);
        });
      });
    });

    context('book does not exist', function() {
      it('should return an error saying that book to update does not exist', function() {
        var req = {
          body: {
            title: 'A Different Title'
          },
          param: function(name) {
            if (name === 'id') {
              return '3';
            } else {
              return undefined;
            }
          }
        };
        var execStub = {
          exec: function(cb) {
            return cb(null, []);
          }
        };

        sandbox.stub(Book, 'update', function(bookToUpdate, newBookInfo) {
          expect(bookToUpdate).to.deep.equal({id: req.param('id')});
          expect(newBookInfo).to.deep.equal(req.body);
          return execStub;
        });

        var errorSpy = sandbox.spy(res, 'badRequest');

        BookController.updateBook(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(sails.config.errors.nonexistentEntry);
      });
    });
  });

  describe('#addGenreToBook', function() {
    context('book to add to exists', function() {
      context('genre to be added exists', function() {
        context('genre to be added has not been added yet', function() {
          it('should return the id of the genre added', function() {
            var genre = {id: '2'};
            var req = {
              param: function(name) {
                if (name === 'bookId') {
                  return '1';
                } else {
                  return undefined;
                }
              },
              body: {
                genreId: '2'
              }
            };
            var book = {
              genres: {
                add: function() {}
              },
              save: function(cb) { return cb(null); }
            };
            sandbox.stub(Book, 'findOne', function(criteria){
              expect(criteria).to.deep.equal({id: req.param('bookId')});
              return {
                exec: function(cb) {
                  return cb(null, book);
                }
              };
            });
            sandbox.stub(Genre, 'findOne', function(criteria) {
              expect(criteria).to.deep.equal({id: req.body.genreId});
              return {
                exec: function(cb) {
                  return cb(null, genre);
                }
              };
            });

            var addSpy = sandbox.spy(book.genres, 'add');
            var saveSpy = sandbox.spy(book, 'save');
            var createdSpy = sandbox.spy(res, 'created');

            BookController.addGenreToBook(req, res);

            addSpy.should.be.calledOnce;
            addSpy.should.be.calledWith(req.body.genreId);

            saveSpy.should.be.calledOnce;

            createdSpy.should.be.calledOnce;
            createdSpy.should.be.calledWith({'genreId': req.body.genreId});
          });
        });
      });
    });
  });

  describe('#deleteGenreFromBook', function() {
    context('genre to be removed is valid and is associated with the given book', function() {
      it('should return a message indicating successful deletion', function() {
        var genre = {id: '2'};
        var req = {
          param: function(name) {
            if (name === 'bookId') {
              return '1';
            } else if (name === 'genreId') {
              return '2';
            } else {
              return undefined;
            }
          }
        };
        var book = {
          genres: {
            remove: function() {}
          },
          save: function(cb) { return cb(null); }
        };
        sandbox.stub(Book, 'findOne', function(criteria){
          expect(criteria).to.deep.equal({id: req.param('bookId')});
          return {
            populate: function(genres) {
              expect(genres).to.deep.equal('genres');
              return {
                exec: function(cb) {
                  return cb(null, book);
                }
              };
            }
          }
        });

        var removeSpy = sandbox.spy(book.genres, 'remove');
        var saveSpy = sandbox.spy(book, 'save');
        var okSpy = sandbox.spy(res, 'ok');

        BookController.removeGenreFromBook(req, res);

        removeSpy.should.be.calledOnce;
        removeSpy.should.be.calledWith(req.param('genreId'));

        saveSpy.should.be.calledOnce;

        okSpy.should.be.calledOnce;
        okSpy.should.be.calledWith(sails.config.messages.successfulDeletion);
      });
    });
  });

  describe('#deleteBook', function() {
    context('book to delete exists', function() {
      it('should return a message indicating deletion success', function() {
        var req = {
          param: function(name) {
            if (name === 'id') {
              return '-1';
            } else {
              return undefined;
            }
          }
        };

        var deletedBooks = [{}];
        var execStub = {
          exec: function(cb) {
            return cb(null, deletedBooks);
          }
        };
        sandbox.stub(Book, 'destroy', function(criteria) {
          expect(criteria).to.deep.equal({id: req.param('id')});
          return execStub;
        });

        var okSpy = sandbox.spy(res, 'ok');

        BookController.deleteBook(req, res);

        okSpy.should.be.calledOnce;
        okSpy.should.be.calledWith(sails.config.messages.successfulDeletion);
      });
    });

    context('book to delete does not exist', function() {
      it('should return error object saying that the entry to be deleted does not exist', function() {
        var req = {
          param: function(name) {
            if (name === 'id') {
              return '3';
            } else {
              return undefined;
            }
          }
        };
        var execStub = {
          exec: function(cb) {
            return cb(null, []);
          }
        };

        sandbox.stub(Book, 'destroy', function(criteria) {
          expect(criteria).to.deep.equal({id: req.param('id')});
          return execStub;
        });

        var errorSpy = sandbox.spy(res, 'badRequest');

        BookController.deleteBook(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(sails.config.errors.nonexistentEntry);
      });
    });
  });
});
