require('sails-test-helper');

var expect = chai.expect;

var rekuire = require('rekuire');
var PublisherController = rekuire('PublisherController');

describe('PublisherController.js', function() {
  describe('#find', function() {
    var sandbox;
    var res;
    var req;
    var publishers;

    beforeEach(function() {
      sandbox = sinon.sandbox.create();
      res = {
        ok: function() {},
        serverError: function() {}
      };
      req = {};
      publishers = [];
    });

    afterEach(function() {
      sandbox.restore();
    });

    context('successful fetch from the database', function() {
      it('should return the list of publishers', function() {
        var execStub = {
          exec: function(cb) {
            return cb(null, publishers);
          }
        };
        sandbox.stub(Publisher, 'find', function(query) {
          expect(query).to.be.an('undefined');
          return execStub;
        });

        var okSpy = sandbox.spy(res, 'ok');

        PublisherController.find(req, res);

        okSpy.should.be.calledOnce;
        okSpy.should.be.calledWith(publishers);
      });
    });

    context('error while fetching from the database', function() {
      it('should return an error object', function() {
        var errorResponse = {};
        var execStub = {
          exec: function(cb) {
            return cb('Error', null);
          }
        };
        sandbox.stub(Publisher, 'find', function(query) {
          expect(query).to.be.an('undefined');
          return execStub;
        });
        var errorSpy = sandbox.spy(res, 'serverError');

        PublisherController.find(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(sails.config.errors.databaseError);
      });
    });
  });
});
