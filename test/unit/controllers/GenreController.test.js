require('sails-test-helper');

var expect = chai.expect;

var rekuire = require('rekuire');
var GenreController = rekuire('GenreController');

describe('GenreController.js', function() {
  describe('#find', function() {
    var sandbox;
    var res;
    var req;
    var genres;

    beforeEach(function() {
      sandbox = sinon.sandbox.create();
      res = {
        ok: function() {},
        serverError: function() {}
      };
      req = {};
      genres = [];
    });

    afterEach(function() {
      sandbox.restore();
    });

    context('successful fetch from the database', function() {
      it('should return the list of genres', function() {
        var execStub = {
          exec: function(cb) {
            return cb(null, genres);
          }
        };
        sandbox.stub(Genre, 'find', function(query) {
          expect(query).to.be.an('undefined');
          return execStub;
        });

        var okSpy = sandbox.spy(res, 'ok');

        GenreController.find(req, res);

        okSpy.should.be.calledOnce;
        okSpy.should.be.calledWith(genres);
      });
    });

    context('error while fetching from the database', function() {
      it('should return an error object', function() {
        var errorResponse = {};
        var execStub = {
          exec: function(cb) {
            return cb('Error', null);
          }
        };
        sandbox.stub(Genre, 'find', function(query) {
          expect(query).to.be.an('undefined');
          return execStub;
        });

        var errorSpy = sandbox.spy(res, 'serverError');

        GenreController.find(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(sails.config.errors.databaseError);
      });
    });
  });
});
