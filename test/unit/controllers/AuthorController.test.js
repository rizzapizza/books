require('sails-test-helper');

var validate = require('validate.js');

var rekuire = require('rekuire');
var AuthorController = rekuire('AuthorController');

var expect = chai.expect;

describe('AuthorController.js', function() {
  var sandbox;
  var res;
  var serverErrorResponse;

  beforeEach(function() {
    sandbox = sinon.sandbox.create();
    res = {
      ok: function() { },
      serverError: function() { },
      created: function() { },
      badRequest: function() { }
    };
    serverErrorResponse = sails.config.errors.databaseError;
  });

  afterEach(function() {
    sandbox.restore();
  });

  describe('#getListOfAuthors', function() {
    context('succesful fetch from the database', function () {
      var authors;
      var execStub;

      before(function () {
        authors = [{}];
        execStub = {
          exec: function (cb) {
            return cb(null, authors);
          }
        };
      });

      it('should call res.ok with list of authors', function () {
        var req = {
          query: {}
        };

        var criteria = undefined;
        sandbox.stub(AuthorCriteria, 'form', function(query) {
          expect(query).to.deep.equal(req.query);
          return criteria;
        });

        sandbox.stub(Author, 'find', function (inputCriteria) {
          expect(inputCriteria).to.deep.equal(criteria);
          return execStub;
        });

        var okSpy = sandbox.spy(res, 'ok');

        AuthorController.getListOfAuthors(req, res);

        okSpy.should.be.calledOnce;
        okSpy.should.be.calledWith(authors);
      });
    });

    context('error in fetching from the database', function() {
      var execStub;

      before(function() {
        execStub = {
          exec: function(cb) {
            return cb('An error with the database occured.');
          }
        };
      });

      it('should return an error object with code -4', function() {
        var req = {
          param: function(name) {
            return undefined;
          }
        };

        var criteria = undefined;
        sandbox.stub(AuthorCriteria, 'form', function(query) {
          expect(query).to.deep.equal(req.query);
          return criteria;
        });

        sandbox.stub(Author, 'find', function() {
          return execStub;
        });
        var errorSpy = sandbox.spy(res, 'serverError');

        AuthorController.getListOfAuthors(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(serverErrorResponse);
      });
    });
  });

  describe('#getAuthorDetails', function() {
    context('author exists', function() {
      it('should call res.ok with the author matching the given id', function() {
        var req = {
          param: function(name) {
            if (name === 'id') {
              return '1';
            } else {
              return undefined;
            }
          }
        };
        var author = {};
        var execStub = {
          exec: function(cb) {
            return cb(null, author);
          }
        };
        sandbox.stub(Author, 'findOne', function(criteria) {
          expect(criteria).to.deep.equal({id: req.param('id')});
          return execStub;
        });
        var okSpy = sandbox.spy(res, 'ok');

        AuthorController.getAuthorDetails(req, res);

        okSpy.should.be.calledOnce;
        okSpy.should.be.calledWith(author);
      });
    });

    context('author does not exist', function() {
      it('should call res.badRequest with a specific error object', function() {
        var req = {
          param: function(name) {
            if (name === 'id') {
              return '9';
            } else {
              return undefined;
            }
          }
        };
        var author = {};
        var execStub = {
          exec: function(cb) {
            return cb(null, null);
          }
        };
        sandbox.stub(Author, 'findOne', function(criteria) {
          expect(criteria).to.deep.equal({id: req.param('id')});
          return execStub;
        });
        var errorSpy = sandbox.spy(res, 'badRequest');

        AuthorController.getAuthorDetails(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(sails.config.errors.nonexistentEntry);
      });
    });
  });

  describe('#getAuthorBooks', function() {
    context('author exists', function() {
      it('should return list of books', function() {
        var books = [{}];
        var req = {
          param: function(name) {
            if (name === 'id') {
              return '1';
            } else {
              return undefined;
            }
          }
        };
        var execStub = {
          exec: function(cb) {
            return cb(null, books);
          }
        };
        sandbox.stub(Book, 'find', function(criteria) {
          expect(criteria).to.deep.equal({'author': req.param('id')});
          return execStub;
        });

        var okSpy = sandbox.spy(res, 'ok');

        AuthorController.getAuthorBooks(req, res);

        okSpy.should.be.calledOnce;
        okSpy.should.be.calledWith(books);
      });
    });
  });

  describe('#createAuthor', function() {
    context('req.body contains complete and valid parameters', function() {
      it('should return the created author', function() {
        var req = {
          body: {
            name: 'Markus Zusak',
            website: 'http://www.zusakbooks.com'
          }
        };
        var createdAuthor = {};
        var execStub = {
          exec: function(cb) {
            return cb(null, createdAuthor);
          }
        };
        sandbox.stub(Author, 'create', function(authorToCreate) {
          expect(authorToCreate).to.deep.equal(req.body);
          return execStub;
        });
        var createdSpy = sandbox.spy(res, 'created');

        AuthorController.createAuthor(req, res);

        createdSpy.should.be.calledOnce;
        createdSpy.should.be.calledWith(createdAuthor);
      });
    });

    context('req.body is missing a name', function() {
      it('should return bad request error object', function() {
        var req = {
          body: {
            website: 'http://www.acoolwebsite.com'
          }
        };
        var badRequestResponse = {
          'error': {
            'code': '-1',
            'message': 'Missing or invalid parameters',
            'parameters': {
              'field': 'name',
              'description': 'Name can\'t be blank'
            }
          }
        };
        var errorSpy = sandbox.spy(res, 'badRequest');

        AuthorController.createAuthor(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(badRequestResponse);
      });
    });

    context('req.body is missing a name', function() {
      it('should return bad request error object', function() {
        var req = {
          body: {
            name: 'A Cool Author',
            website: 'A Cool Website'
          }
        };
        var badRequestResponse = {
          'error': {
            'code': '-1',
            'message': 'Missing or invalid parameters',
            'parameters': {
              'field': 'website',
              'description': 'Website is not a valid url'
            }
          }
        };
        var errorSpy = sandbox.spy(res, 'badRequest');

        AuthorController.createAuthor(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(badRequestResponse);
      });
    });
  });

  describe('#updateAuthor', function() {
    context('author exists', function() {
      context('req.body has valid and complete parameters', function() {
        it('should call res.ok with the updated book', function () {
          var req = {
            body: {
              name: 'Markus E. Zusak',
              website: 'http://www.zusakbook.com'
            },
            param: function (name) {
              if (name === 'id') {
                return '2';
              } else {
                return undefined;
              }
            }
          };
          var updatedAuthors = [{}];
          var execStub = {
            exec: function (cb) {
              return cb(null, updatedAuthors);
            }
          };
          sandbox.stub(Author, 'update', function (authorToUpdate, newAuthorInfo) {
            expect(authorToUpdate).to.deep.equal({id: '2'});
            expect(newAuthorInfo).to.deep.equal(req.body);
            return execStub;
          });
          var okSpy = sandbox.spy(res, 'ok');

          AuthorController.updateAuthor(req, res);

          okSpy.should.be.calledOnce;
          okSpy.should.be.calledWith(updatedAuthors[0]);
        });
      });

      context('req.body has no valid parameters', function() {
        it('should return an error object', function() {
          var req = {
            body: {
              uselessField: 'Useless field'
            },
            param: function(name) {
              if (name === 'id') {
                return '2';
              } else {
                return undefined;
              }
            }
          };

          var errorSpy = sandbox.spy(res, 'badRequest');

          AuthorController.updateAuthor(req, res);

          errorSpy.should.be.calledOnce;
          errorSpy.should.be.calledWith(sails.config.errors.badRequest);
        });
      });
    });

    context('author to update does not exists', function() {
      it('should return an error saying that author to update does not exist', function() {
        var req = {
          body: {
            name: 'A Name'
          },
          param: function(name) {
            if (name === 'id') {
              return '3';
            } else {
              return undefined;
            }
          }
        };

        var execStub = {
          exec: function(cb) {
            return cb(null, []);
          }
        };

        sandbox.stub(Author, 'update', function(authorToUpdate, newAuthorInfo) {
          expect(authorToUpdate).to.deep.equal({id: req.param('id')});
          expect(newAuthorInfo).to.deep.equal(req.body);
          return execStub;
        });

        var errorSpy = sandbox.spy(res, 'badRequest');

        AuthorController.updateAuthor(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(sails.config.errors.nonexistentEntry);
      });
    });
  });

  describe('#deleteAuthor', function() {
    context('author exists', function() {
      it('should return a message indicating deletion success', function() {
        var req = {
          param: function(name) {
            if (name === 'id') {
              return '1';
            } else {
              return undefined;
            }
          }
        };

        var deletedAuthors = [{}];
        var execStub = {
          exec: function(cb) {
            return cb(null, deletedAuthors);
          }
        };
        sandbox.stub(Author, 'destroy', function(criteria) {
          expect(criteria).to.deep.equal({id: req.param('id')});
          return execStub;
        });

        var okSpy = sandbox.spy(res, 'ok');

        AuthorController.deleteAuthor(req, res);

        okSpy.should.be.calledOnce;
        okSpy.should.be.calledWith(sails.config.messages.successfulDeletion);
      });
    });

    context('author to delete does not exist', function() {
      it('should return error object saying that the entry to be deleted does not exist', function() {
        var req = {
          param: function(name) {
            if (name === 'id') {
              return '3';
            } else {
              return undefined;
            }
          }
        };

        var execStub = {
          exec: function(cb) {
            return cb(null, []);
          }
        };

        sandbox.stub(Author, 'destroy', function(criteria) {
          expect(criteria).to.deep.equal({id: req.param('id')});
          return execStub;
        });

        var errorSpy = sandbox.spy(res, 'badRequest');

        AuthorController.deleteAuthor(req, res);

        errorSpy.should.be.calledOnce;
        errorSpy.should.be.calledWith(sails.config.errors.nonexistentEntry);
      });
    });
  });

});
