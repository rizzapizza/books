require('sails-test-helper');

var expect = chai.expect;
var passport = require('passport');

describe('Facebook.js', function() {
  describe('#init', function() {
    var sandbox;

    beforeEach(function() {
      sandbox = sinon.sandbox.create();
    });

    afterEach(function() {
      sandbox.restore();
    });

    it('should call passport.serializeUser', function() {
      var spy = sandbox.spy(passport, 'serializeUser');
      Facebook.init();
      spy.should.be.calledOnce;
    });

    it('should call passport.deserializeUser', function() {
      var spy = sandbox.spy(passport, 'deserializeUser');
      Facebook.init();
      spy.should.be.calledOnce;
    });

    it('should call passport.use', function() {
      var spy = sandbox.spy(passport, 'use');
      Facebook.init();
      spy.should.be.calledOnce;
    });
  });
});