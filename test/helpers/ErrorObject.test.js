require('sails-test-helper');

var expect = chai.expect;

describe('ErrorObject.js', function() {
  describe('#generate', function() {
    context('with valid code', function() {
      context('without message as input parameter', function() {
        it('should return the correct error object', function() {
          expect(ErrorObject.customize(sails.config.errors.badRequest)).to.deep.equal({
            'error': {
              'code': '-1',
              'message': 'Missing or invalid parameters'
            }
          });
        });
      });

      context('with message as input parameter', function() {
        it('should return an error object with the custom message', function() {
          expect(ErrorObject.customize(sails.config.errors.badRequest, 'Custom error message')).to.deep.equal({
            'error': {
              'code': '-1',
              'message': 'Custom error message'
            }
          });
        });
      });

      context('with validation error object as input parameter', function() {
        it('should return an error object with field and description', function() {
          expect(ErrorObject.customize(sails.config.errors.badRequest, 'Custom error message', {'title': ['Title can\'t be blank']})).to.deep.equal({
            'error': {
              'code': '-1',
              'message': 'Custom error message',
              'parameters': {
                'field': 'title',
                'description': 'Title can\'t be blank'
              }
            }
          });
        });
      });
    });

    context('with invalid code', function() {
      it('should return null', function() {
        expect(ErrorObject.customize(sails.config.errors.doesntExist)).to.be.a('null');
      });
    });
  });
});
