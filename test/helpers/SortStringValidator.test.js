require('sails-test-helper');

var expect = chai.expect;

describe('SortStringValidator.js', function() {
  describe('#book.validate', function() {
    context('undefined input', function() {
      it('should return undefined', function() {
        var result = SortStringValidator.validate.book(undefined);
        expect(result).to.be.an('undefined');
      });
    });

    context('one pair of valid parameters', function() {
      context('title parameter with order ASC', function() {
        it('should return no error', function() {
          var result = SortStringValidator.validate.book('title', 'ASC');
          expect(result).to.be.an('undefined');
        });
      });

      context('title parameter with order DESC', function() {
        it('should return no error', function() {
          var result = SortStringValidator.validate.book('title', 'DESC');
          expect(result).to.be.an('undefined');
        });
      });

      context('author parameter with order ASC', function() {
        it('should return no error', function() {
          var result = SortStringValidator.validate.book('author', 'ASC');
          expect(result).to.be.an('undefined');
        });
      });

      context('publisher parameter with order ASC', function() {
        it('should return no error', function() {
          var result = SortStringValidator.validate.book('publisher', 'ASC');
          expect(result).to.be.an('undefined');
        });
      });
    });

    context('invalid request', function() {
      var sandbox;
      var errorResponse;

      beforeEach(function() {
        sandbox = sinon.sandbox.create();
        errorResponse = {};
      });

      afterEach(function() {
        sandbox.restore();
      });

      context('invalid field', function() {
        it('should return an error saying that the field should be valid', function(){
          sandbox.stub(ErrorObject, 'customize', function(errorObject, message, validationError) {
            expect(errorObject).to.deep.equal(sails.config.errors.badRequest);
            expect(message).to.be.a('null');
            expect(validationError).to.deep.equal({'sort': ['name needs to be in title,author,publisher']});
            return errorResponse;
          });
          var result = SortStringValidator.validate.book('name', 'DESC');
          expect(result).to.deep.equal(errorResponse);
        });

        it('should return an error saying that the order should be valid', function(){
          sandbox.stub(ErrorObject, 'customize', function(errorObject, message, validationError) {
            expect(errorObject).to.deep.equal(sails.config.errors.badRequest);
            expect(message).to.be.a('null');
            expect(validationError).to.deep.equal({'sort': ['DC needs to be in asc,desc']});
            return errorResponse;
          });
          var result = SortStringValidator.validate.book('title', 'DC');
          expect(result).to.deep.equal(errorResponse);
        });
      });
    });
  });

  describe('#author.validate', function() {
    context('undefined input', function() {
      it('should return undefined', function() {
        var result = SortStringValidator.validate.author(undefined);
        expect(result).to.be.an('undefined');
      });
    });

    context('one pair of valid parameters', function() {
      context('name parameter with order ASC', function () {
        it('should return no error', function () {
          var result = SortStringValidator.validate.author('name', 'ASC');
          expect(result).to.be.an('undefined');
        });
      });

      context('website parameter with order DESC', function () {
        it('should return no error', function () {
          var result = SortStringValidator.validate.author('website', 'DESC');
          expect(result).to.be.an('undefined');
        });
      });
    });

    context('invalid request', function() {
      var sandbox;
      var errorResponse;

      beforeEach(function() {
        sandbox = sinon.sandbox.create();
        errorResponse = {};
      });

      afterEach(function() {
        sandbox.restore();
      });

      context('invalid field', function() {
        it('should return an error saying that the field should be valid', function(){
          sandbox.stub(ErrorObject, 'customize', function(errorObject, message, validationError) {
            expect(errorObject).to.deep.equal(sails.config.errors.badRequest);
            expect(message).to.be.a('null');
            expect(validationError).to.deep.equal({'sort': ['title needs to be in name,website']});
            return errorResponse;
          });
          var result = SortStringValidator.validate.author('title', 'DESC');
          expect(result).to.deep.equal(errorResponse);
        });
      });
    });
  });
});
