require('sails-test-helper');

var expect = chai.expect;

describe('BookSqlQuery.js', function() {
  var baseQuery = 'SELECT book.id AS id, book.title AS title, author.name AS author, publisher.name AS publisher, book.isbn10 AS isbn10, book.isbn13 AS isbn13, book."publishedAt" AS "publishedAt" FROM book INNER JOIN author ON book.author = author.id INNER JOIN publisher ON book.publisher = publisher.id';

  describe('#form', function() {
    context('no search criteria', function() {
      it('should return select statement without where condition', function() {
        var query = {};
        expect(BookSqlQuery.form(query)).to.deep.equal({text: baseQuery + ' ORDER BY book.title', values: []});
      });
    });

    context('with one search criteria', function() {
      context('with title search criteria', function() {
        it('should return statement with where title like condition', function() {
          var title = 'title';
          var query = {title: title};
          expect(BookSqlQuery.form(query)).to.deep.equal({text: baseQuery + ' WHERE LOWER(title) LIKE LOWER($1) ORDER BY book.title', values: ['%' + title + '%']});
        });
      });

      context('with author search criteria', function() {
        it('should return statement with where author in condition', function() {
          var author = 'author';
          var query = {author: author};
          expect(BookSqlQuery.form(query)).to.deep.equal({text: baseQuery + ' WHERE author IN (SELECT author.id FROM author WHERE LOWER(author.name) LIKE LOWER($1)) ORDER BY book.title', values: ['%' + author + '%']})
        });
      });

      context('with publisher search criteria', function() {
        it('should return statement with where publisher in condition', function() {
          var publisher = 'publisher';
          var query = {publisher: publisher};
          expect(BookSqlQuery.form(query)).to.deep.equal({text: baseQuery + ' WHERE publisher IN (SELECT publisher.id FROM publisher WHERE LOWER(publisher.name) LIKE LOWER($1)) ORDER BY book.title', values: ['%' + publisher + '%']});
        });
      });

      context('with genre search criteria', function() {
        it('should return statement with where genre in condition', function() {
          var genre = 'genre';
          var query = {genre: genre};
          expect(BookSqlQuery.form(query)).to.deep.equal({text: baseQuery + ' WHERE book.id IN (SELECT book_genres__genre_books.book_genres FROM book_genres__genre_books WHERE book_genres__genre_books.genre_books IN (SELECT genre.id FROM genre WHERE LOWER(genre.name) LIKE LOWER($1))) ORDER BY book.title', values: ['%' + genre + '%']});
        });
      });

      context('with sort criteria', function() {
        it('should return statement with order by', function() {
          var query = {sort: 'title'};
          expect(BookSqlQuery.form(query)).to.deep.equal({text: baseQuery + ' ORDER BY book.title', values: []});
        });
      });

      context('with skip criteria', function() {
        it('should return statement with order by', function() {
          var query = {skip: '2'};
          expect(BookSqlQuery.form(query)).to.deep.equal({text: baseQuery + ' ORDER BY book.title OFFSET $1', values: ['2']});
        });
      });

      context('with limit criteria', function() {
        it('should return statement with order by', function() {
          var query = {limit: '2'};
          expect(BookSqlQuery.form(query)).to.deep.equal({text: baseQuery + ' ORDER BY book.title LIMIT $1', values: ['2']});
        });
      });
    });

    context('with more than one search criteria', function() {
      it('should return statement with two where conditions separated by an end', function() {
        var title = 'title';
        var author = 'author';
        var query = {title: title, author: author};
        expect(BookSqlQuery.form(query)).to.deep.equal({text: baseQuery + ' WHERE LOWER(title) LIKE LOWER($1) AND author IN (SELECT author.id FROM author WHERE LOWER(author.name) LIKE LOWER($2)) ORDER BY book.title', values: ['%' + title + '%', '%' + author + '%']});
      });
    });
  });
});
