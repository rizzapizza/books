require('sails-test-helper');

var expect = chai.expect;

describe('AuthorCriteria.js', function() {
  describe('#form', function() {
    var sandbox;
    var defaultCriteria;

    before(function() {
      defaultCriteria = {'sort': 'name'};
    });

    afterEach(function() {
      sandbox.restore();
    });

    context('without sort string', function(){
      beforeEach(function() {
        sandbox = sinon.sandbox.create();
        sandbox.stub(SortStringValidator.validate, 'author', function(sortBy, sortType) {
          expect(sortBy).to.be.an('undefined');
          expect(sortType).to.be.an('undefined');
          return undefined;
        });
      });

      context('without any search criteria', function() {
        it('should return undefined', function() {
          var query = {};
          expect(AuthorCriteria.form(query)).to.deep.equal(defaultCriteria);
        });
      });

      context('with one search criteria', function() {
        context('with name criteria', function() {
          it('should return name: {like: %string%}', function() {
            var query = {name: 'string'};
            expect(AuthorCriteria.form(query)).to.deep.equal({'sort': 'name', 'name': {'like': '%string%'}});
          });
        });

        context('with website criteria', function() {
          it('should return website: {like: %string%}', function() {
            var query = {website: 'string'};
            expect(AuthorCriteria.form(query)).to.deep.equal({'sort': 'name', 'website': {'like': '%string%'}});
          });
        });
      });

      context('with more than one search criteria', function() {
        it('should return {name: {like: %string%}, website: {like: %string%}}', function() {
          var query = {name: 'name', website: 'website'};
          expect(AuthorCriteria.form(query)).to.deep.equal({'sort': 'name', 'name': {'like': '%name%'}, 'website': {'like': '%website%'}});
        });
      });
    });

    context('with sort string', function() {
      beforeEach(function() {
        sandbox = sinon.sandbox.create();
        sandbox.stub(SortStringValidator.validate, 'author', function(sortBy, sortType) {
          expect(sortBy).to.deep.equal('name');
          expect(sortType).to.deep.equal('ASC');
          return undefined;
        });
      });

      context('with name criteria', function() {
        it('should return name: {like: %string%}, sort: name ASC', function() {
          var query = {name: 'string', sortBy: 'name', sortType: 'ASC'};
          expect(AuthorCriteria.form(query)).to.deep.equal({'sort': 'name ASC', 'name': {'like': '%string%'}});
        });
      });
    });
  });
});
