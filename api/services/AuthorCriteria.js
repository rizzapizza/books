var validate = require('validate.js');

module.exports = {
  form: function(query) {
    var params = validate.cleanAttributes(query, sails.config.whitelists.authorGetParams);
    var validationError = validate.validate(params, sails.config.constraints.sort);
    if (validationError) {
      return ErrorObject.customize(sails.config.errors.badRequest, null, validationError);
    }

    var sortStringError = SortStringValidator.validate.author(params.sortBy, params.sortType);
    if (sortStringError && sortStringError.error) {
      return sortStringError;
    }

    if (_.isEmpty(params)) {
      return {'sort': 'name'};
    }
    var criteria = {'sort': 'name'};
    for (var name in params) {
      if (sails.config.whitelists.sortFields.indexOf(name) === -1) {
        criteria[name] = {'like': '%' + params[name] + '%'};
      } else if (name != 'sortBy' && name != 'sortType') {
        criteria[name] = params[name];
      }
    }
    if (params.sortBy) {
      criteria.sort = params.sortBy;
    }
    if (params.sortType) {
      criteria.sort += (' ' + params.sortType);
    }
    return criteria;
  }
};
