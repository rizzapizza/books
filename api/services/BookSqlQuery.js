var validate = require('validate.js');

module.exports = {
  form: function(query) {
    var params = validate.cleanAttributes(query, sails.config.whitelists.bookGetParams);

    var validationError = validate.validate(params, sails.config.constraints.sort);

    if (validationError) {
      return ErrorObject.customize(sails.config.errors.badRequest, null, validationError);
    }

    var sortStringError = SortStringValidator.validate.book(params.sortBy, params.sortType);
    if (sortStringError && sortStringError.error) {
      return sortStringError;
    }

    var sqlQuery = 'SELECT' +
      ' book.id AS id, book.title AS title, author.name AS author, publisher.name AS publisher,' +
      ' book.isbn10 AS isbn10, book.isbn13 AS isbn13, book."publishedAt" AS "publishedAt"' +
      ' FROM book' +
      ' INNER JOIN author' +
      ' ON book.author = author.id' +
      ' INNER JOIN publisher' +
      ' ON book.publisher = publisher.id';
    var sortParams = {};
    var values = [];
    var i = 1;
    for (var name in params) {
      if (sails.config.whitelists.sortFields.indexOf(name) > -1) {
        sortParams[name] = params[name];
        continue;
      }

      if (i > 1) {
        sqlQuery += ' AND';
      } else {
        sqlQuery += ' WHERE';
      }

      params[name] = '%' + params[name] + '%';

      if (name === 'title') {
        sqlQuery += ' LOWER(' + name + ') LIKE LOWER($' + i.toString() + ')';
      } else if (name === 'genre') {
        sqlQuery += ' book.id IN (SELECT book_genres__genre_books.book_genres FROM book_genres__genre_books WHERE book_genres__genre_books.genre_books IN (SELECT genre.id FROM ' + name + ' WHERE LOWER(genre.name) LIKE LOWER($' + i.toString() + ')))';
      } else {
        sqlQuery += ' ' + name +' IN (SELECT ' + name + '.id FROM ' + name + ' WHERE LOWER(' + name + '.name) LIKE LOWER($' + i.toString() + '))';
      }

      values.push(params[name]);
      i++;
    }

    if (sortParams.sortBy) {
      sqlQuery += ' ORDER BY ' + sortParams.sortBy;
    } else {
      sqlQuery += ' ORDER BY book.title';
    }
    if (sortParams.sortType) {
      sqlQuery += ' ' + sortParams.sortType;
    }
    if (sortParams.skip) {
      sqlQuery += ' OFFSET $' + i.toString();
      values.push(sortParams.skip);
      i++;
    }
    if (sortParams.limit) {
      sqlQuery += ' LIMIT $' + i.toString();
      values.push(sortParams.limit);
      i++;
    }

    return {text: sqlQuery, values: values};
  }
};
