function validateSortStrings(sortBy, sortType, fields) {
  if (sortBy && fields.indexOf(sortBy.toLowerCase()) == -1) {
    return ErrorObject.customize(sails.config.errors.badRequest, null, {'sort': [sortBy + ' needs to be in ' + fields]});
  } else if (sortType && sails.config.whitelists.orders.indexOf(sortType.toLowerCase()) == -1) {
    return ErrorObject.customize(sails.config.errors.badRequest, null, {'sort': [sortType + ' needs to be in ' + sails.config.whitelists.orders]});
  }
  return undefined;
}

module.exports = {
  validate: {
    book: function(sortBy, sortType) {
      return validateSortStrings(sortBy, sortType, sails.config.whitelists.bookFields);
    },
    author: function(sortBy, sortType) {
      return validateSortStrings(sortBy, sortType, sails.config.whitelists.authorFields);
    }
  }
};
