var passport = require('passport'),
  FacebookStrategy = require('passport-facebook').Strategy;

module.exports = {
  verifyUser: function(token, tokenSecret, profile, done) {
    User.findOne({uid: profile.id}, function (err, user) {
      if (user) {
        return done(null, user);
      } else {

        var data = {
          provider: profile.provider,
          uid: profile.id,
          name: profile.displayName
        };

        if (profile.emails && profile.emails[0] && profile.emails[0].value) {
          data.email = profile.emails[0].value;
        }
        if (profile.name && profile.name.givenName) {
          data.firstname = profile.name.givenName;
        }
        if (profile.name && profile.name.familyName) {
          data.lastname = profile.name.familyName;
        }

        User.create(data, function (err, user) {
          return done(err, user);
        });
      }
    });
  },

  init: function() {
    passport.serializeUser(function(user, done) {
      done(null, user.uid);
    });

    passport.deserializeUser(function(uid, done) {
      User.findOne({uid: uid}, done);
    });

    passport.use(new FacebookStrategy({
      clientID: sails.config.facebook.clientID,
      clientSecret: sails.config.facebook.clientSecret,
      callbackURL: sails.config.facebook.callbackURL
    }, this.verifyUser));
  }
};