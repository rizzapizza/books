module.exports = {
  customize: function(defaultErrorObject, message, validationError) {
    if (defaultErrorObject) {
      var errorObject = {'error': {}};
      for (var field in defaultErrorObject.error) {
        errorObject.error[field] = defaultErrorObject.error[field];
      }
      if (message) {
        errorObject.error.message = message;
      }
      if (validationError) {
        var field = Object.keys(validationError)[0];
        var description = validationError[field][0];
        if (field && description) {
          errorObject.error['parameters'] = {}
          errorObject.error.parameters['field'] = field;
          errorObject.error.parameters['description'] = description;
        }
      }
      return errorObject;
    } else {
      return null;
    }
  }

};
