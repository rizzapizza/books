/**
 * GenreController
 *
 * @description :: Server-side logic for managing genres
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	find: function(req, res) {
    Genre.find().exec(function(err, genres) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      }
      return res.ok(genres);
    });
  }
};

