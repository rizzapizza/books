/**
 * AuthorController
 *
 * @description :: Server-side logic for managing authors
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var validate = require('validate.js');

module.exports = {
  getAuthorDetails: function(req, res) {
    Author.findOne({id: req.param('id')}).exec(function(err, author) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      } else if (!author) {
        return res.badRequest(sails.config.errors.nonexistentEntry);
      }
      return res.ok(author);
    });
  },
  getAuthorBooks: function(req, res) {
    Book.find({author: req.param('id')}).exec(function(err, books) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      }
      return res.ok(books);
    });
  },
	getListOfAuthors: function(req, res) {
    var criteria = AuthorCriteria.form(req.query);
    if (criteria && criteria.error) {
      return res.badRequest(criteria);
    }
    Author.find(criteria).exec(function(err, authors) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      }
      return res.ok(authors);
    });
  },
  createAuthor: function(req, res) {
    var authorToCreate = validate.cleanAttributes(req.body, sails.config.whitelists.authorPostParams);
    var validationError = validate.validate(authorToCreate, sails.config.constraints.author);
    if (validationError) {
      return res.badRequest(ErrorObject.customize(sails.config.errors.badRequest, null, validationError));
    }
    Author.create(authorToCreate).exec(function(err, createdAuthor) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      }
      return res.created(createdAuthor);
    });
  },
  updateAuthor: function(req, res) {
    var authorToUpdate = validate.cleanAttributes(req.body, sails.config.whitelists.authorPostParams);
    var validationError = validate.validate(authorToUpdate, sails.config.constraints.updateAuthor);
    if (Object.keys(authorToUpdate).length == 0) {
      return res.badRequest(sails.config.errors.badRequest);
    } else if (validationError) {
      return res.badRequest(ErrorObject.customize(sails.config.errors.badRequest, null, validationError));
    }
    Author.update({id: req.param('id')}, authorToUpdate).exec(function(err, updatedAuthors) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      } else if (updatedAuthors.length === 0) {
        return res.badRequest(sails.config.errors.nonexistentEntry);
      }
      return res.ok(updatedAuthors[0]);
    });
  },
  deleteAuthor: function(req, res) {
    Author.destroy({id: req.param('id')}).exec(function(err, deletedAuthors) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      } else if (deletedAuthors.length === 0) {
        return res.badRequest(sails.config.errors.nonexistentEntry);
      }
      return res.ok(sails.config.messages.successfulDeletion);
    });
  }
};

