/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var passport = require('passport');

module.exports = {
	login: function(req, res) {
	  if (req.isAuthenticated()) {
	    return res.redirect('/admin/books');
    }
	  return res.view('login');
  },
  logout: function(req, res) {
    req.logout();
    return res.redirect('/login');
  },
  facebook: function (req, res, next) {
    passport.authenticate('facebook', { scope: ['email']},
      function (err, user) {
      })(req, res, next);
  },
  callback: function (req, res, next) {
    passport.authenticate('facebook',
      function (err, user) {
        req.logIn(user, function (err) {
          if(err) {
            sails.log.error(err);
            res.view('500');
            return;
          }
          res.redirect('/admin/books');
          return;
        });
      })(req, res, next);
  }
};

