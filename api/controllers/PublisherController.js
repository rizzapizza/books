/**
 * PublisherController
 *
 * @description :: Server-side logic for managing publishers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  find: function(req, res) {
    Publisher.find().exec(function(err, publishers) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      }
      return res.ok(publishers);
    });
  }
};

