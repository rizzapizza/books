module.exports = {
  books: function(req, res) {
    return res.view('books');
  },
  authors: function(req, res) {
    return res.view('authors');
  }
};