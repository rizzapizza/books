/**
 * BookController
 *
 * @description :: Server-side logic for managing books
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var validate = require('validate.js');
var moment = require('moment');

validate.extend(validate.validators.datetime, {
  parse: function(value, options) {
    return +moment.utc(value);
  },
  format: function(value, options) {
    var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
    return moment.utc(value).format(format);
  }
});
validate.validators.notEmpty = function(value, options, key, attributes) {
  if (value instanceof Array && value.length === 0) {
    return "must not be empty";
  }
  return undefined;
};

module.exports = {
  getBook: function(req, res) {
    Book.findOne({id: req.param('id')})
      .populate('author')
      .populate('publisher')
      .populate('genres')
      .exec(function(err, book) {
        if (err) {
          return res.serverError(sails.config.errors.databaseError);
        } else if (!book) {
          return res.badRequest(sails.config.errors.nonexistentEntry);
        }
        return res.ok(book);
    });
  },
  getListOfBooks: function(req, res) {
    var query = BookSqlQuery.form(req.query);
    if (query && query.error) {
      return res.badRequest(query);
    }
    Book.query(query, function(err, results) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      }
      return res.ok(results.rows);
    });
  },
  createBook: function(req, res) {
    var bookToCreate = validate.cleanAttributes(req.body, sails.config.whitelists.bookPostParams);
    var validationError = validate.validate(bookToCreate, sails.config.constraints.book);
    if (validationError) {
      return res.badRequest(ErrorObject.customize(sails.config.errors.badRequest, null, validationError));
    }
    Book.create(bookToCreate).exec(function(err, createdBook) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      }
      return res.created(createdBook);
    });
  },
  addGenreToBook: function(req, res) {
    if (!req.body.genreId) {
      return res.badRequest(sails.config.errors.badRequest);
    }
    Book.findOne({id: req.param('bookId')}).exec(function(err, book) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      } else if (!book) {
        return res.badRequest(sails.config.errors.nonexistentEntry);
      }
      Genre.findOne({id: req.body.genreId}).exec(function(err, genre) {
        if (err) {
          return res.serverError(sails.config.errors.databaseError);
        } else if (!genre) {
          return res.badRequest(sails.config.errors.nonexistentEntry);
        }
        book.genres.add(genre.id);
        book.save(function(err) {
          if (err) {
            return res.serverError(sails.config.errors.databaseError);
          }
          return res.created({'genreId': genre.id});
        });
      });
    });
  },
  removeGenreFromBook: function(req, res) {
    Book.findOne({id: req.param('bookId')}).populate('genres').exec(function(err, book) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      } else if (!book) {
        return res.badRequest(sails.config.errors.nonexistentEntry);
      }
      book.genres.remove(req.param('genreId'));
      book.save(function(err) {
        if (err) {
          return res.serverError(sails.config.errors.databaseError);
        }
        return res.ok(sails.config.messages.successfulDeletion);
      });
    });
  },
  updateBook: function(req, res) {
    var bookToUpdate = validate.cleanAttributes(req.body, sails.config.whitelists.bookPostParams);
    var validationError = validate.validate(bookToUpdate, sails.config.constraints.updateBook);
    if (Object.keys(bookToUpdate).length == 0) {
      return res.badRequest(sails.config.errors.badRequest);
    } else if (validationError) {
      return res.badRequest(ErrorObject.customize(sails.config.errors.badRequest, null, validationError));
    }
    Book.update({id: req.param('id')}, bookToUpdate).exec(function(err, updatedBooks) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      } else if (updatedBooks.length === 0) {
        return res.badRequest(sails.config.errors.nonexistentEntry);
      }
      return res.ok(updatedBooks[0]);
    });
  },
  deleteBook: function(req, res) {
    Book.destroy({id: req.param('id')}).exec(function(err, deletedBooks) {
      if (err) {
        return res.serverError(sails.config.errors.databaseError);
      } else if (deletedBooks.length === 0) {
        return res.badRequest(sails.config.errors.nonexistentEntry);
      }
      return res.ok(sails.config.messages.successfulDeletion);
    });
  }
};

