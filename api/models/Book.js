/**
 * Book.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  connection: 'postgresqlServer',
  attributes: {
    title: {
      type: 'string',
      required: 'true',
      size: 50
    },
    author: {
      model: 'author',
      required: 'true'
    },
    publisher: {
      model: 'publisher',
      required: 'true'
    },
    publishedAt: {
      type: 'date',
      required: 'true'
    },
    isbn10: {
      type: 'string',
      required: true,
      size: 10
    },
    isbn13: {
      type: 'string',
      required: true,
      size: 13
    },
    genres: {
      collection: 'genre',
      via: 'books'
    },
    toJSON: function() {
      var obj = this.toObject();
      delete obj.createdAt;
      delete obj.updatedAt;
      return obj;
    }
  }


};

