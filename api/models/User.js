/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  connection: 'mongodbServer',

  attributes: {
    'provider': {
      'type': 'string'
    },
    'uid': {
      'type': 'string'
    },
    'name': {
      'type': 'string'
    },
    'email': {
      'type': 'string'
    },
    'firstname': {
      'type': 'string'
    },
    'lastname': {
      'type': 'string'
    }
  }
};

