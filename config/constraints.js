module.exports.constraints = {
  sort: {
    'limit': {
      numericality: {
        onlyInteger: true,
      }
    },
    'skip': {
      numericality: {
        onlyInteger: true
      }
    }
  },
  book: {
    'title': {
      presence: true,
      length: {maximum: 50},
    },
    'author': {
      presence: true,
      numericality: {
        onlyInteger: true
      }
    },
    'publisher': {
      presence: true,
      numericality: {
        onlyInteger: true
      }
    },
    'genres': {
      presence: true
    },
    'publishedAt': {
      presence: true,
      datetime: {
        dateOnly: true
      }
    },
    'isbn10': {
      presence: true,
      length: {is: 10},
      numericality: {
        onlyInteger: true
      }
    },
    'isbn13': {
      presence: true,
      length: {is: 13},
      numericality: {
        onlyInteger: true
      }
    }
  },
  updateBook: {
    'title': {
      length: {maximum: 50},
    },
    'author': {
      numericality: {
        onlyInteger: true
      }
    },
    'publisher': {
      numericality: {
        onlyInteger: true
      }
    },
    'publishedAt': {
      datetime: {
        dateOnly: true
      }
    },
    'isbn10': {
      length: {is: 10},
      numericality: {
        onlyInteger: true
      }
    },
    'isbn13': {
      length: {is: 13},
      numericality: {
        onlyInteger: true
      }
    },
    'genres': {
      notEmpty: true
    }
  },
  author: {
    'name': {
      presence: true,
      length: {maximum: 50}
    },
    'website': {
      url: true,
      length: {maximum: 50}
    }
  },
  updateAuthor: {
    'name': {
      length: {maximum: 50}
    },
    'website': {
      url: true,
      length: {maximum: 50}
    }
  }
};
