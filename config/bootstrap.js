/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)

  // async.auto({
  //   createAuthor: function(cb) {
  //     Author.create({
  //       name: 'J.K. Rowling',
  //       website: 'http://www.jkrowling.com'
  //     }).exec(cb);
  //   },
  //   createGenre1: function(cb) {
  //     Genre.create({
  //       name: 'Fantasy'
  //     }).exec(cb);
  //   },
  //   createGenre2: function(cb) {
  //     Genre.create({
  //       name: 'Horror'
  //     }).exec(cb);
  //   },
  //   createPublisher: function(cb) {
  //     Publisher.create({
  //       name: 'Scholastic'
  //     }).exec(cb);
  //   },
  //   createBook: [ 'createAuthor', 'createGenre1', 'createGenre2', 'createPublisher', function(cb, result) {
  //     Book.create({
  //       title: 'Harry Potter',
  //       author: result.createAuthor.id,
  //       publisher: result.createPublisher.id,
  //       genres: [result.createGenre1.id, result.createGenre2.id],
  //       publishedAt: '2015-01-01',
  //       isbn10: 'aaaaaaaaaa',
  //       isbn13: 'aaaaaaaaaaaaa'
  //     }).exec(cb);
  //   }]
  // }, cb);
  cb();
};
