module.exports.whitelists = {
  sortFields: ['sortBy', 'sortType', 'skip', 'limit'],
  orders: ['asc', 'desc'],
  bookFields: ['title', 'author', 'publisher'],
  authorFields: ['name', 'website'],
  bookGetParams: {
    'title': true,
    'author': true,
    'publisher': true,
    'genre': true,
    'limit': true,
    'skip': true,
    'sortBy': true,
    'sortType': true
  },
  bookPostParams: {
    'title': true,
    'author': true,
    'publisher': true,
    'publishedAt': true,
    'isbn10': true,
    'isbn13': true,
    'genres': true
  },
  authorGetParams: {
    'name': true,
    'website': true,
    'limit': true,
    'skip': true,
    'sortBy': true,
    'sortType': true
  },
  authorPostParams: {
    'name': true,
    'website': true
  }

};
