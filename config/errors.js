module.exports.errors = {
  badRequest: {
    'error': {
      'code': '-1',
      'message': 'Missing or invalid parameters'
    }
  },
  nonexistentEntry: {
    'error': {
      'code': '-2',
      'message': 'Model entry with given ID does not exist'
    }
  },
  unauthorized: {
    'error': {
      'code': '-3',
      'message': 'Unauthorized access'
    }
  },
  notFound: {
    'error': {
      'code': '-4',
      'message': 'Content not found'
    }
  },
  internalServerError: {
    'error': {
      'code': '-5',
      'message': 'Internal server error'
    }
  },
  databaseError: {
    'error': {
      'code': '-6',
      'message': 'An error while interacting with the database occured'
    }
  }
};
